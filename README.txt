#####[ StreamPi v1.0.8 ]#####

Author: TheOuterLinux
Release Date: 2022/02/10
Website: https://theouterlinux.gitlab.io

#####[ ABOUT ]#####

StreamPi is designed to help people with older x86 computers and lighter ARM devices that cannot run newer screen-casting or live streaming tools because of higher-end graphics-related requirements.

Originally, StreamPi was created to run from the command-line on a Raspberry Pi (hence the name) using dialog as a TUI (Terminal User Interface) with the ability to run both in X AND console (with limitations). However, I do realize that not everyone is comfortable with the command-line. So, I decided to create an actual GUI (Graphical User Interface) version this time around. The interface should run fine as long as your system has GAMBAS available.

Note-worthy features:
    * Monitor Twitch and Picarto stream stats
      without needing authentication (read-only)
    * Image Overlay (see after recording or while 
      monitoring live stream)
    * Use notify-send to display IRC** log chat as
      well as Follow alerts
    * Auto translate IRC** chat displayed with
      notify-send
    * Store up to 3 RTMP urls
    * Torify your live stream for extra privacy
    * Crop-out parts of your screen to hide things
      such as panels
    * Record or live stream all connected and
      enabled monitors all in one go
    * Automatically uses current sound server such
      as PulseAudio, ALSA, or JACK
    * Quickly launch a music playing program with
      selected folder or playlist and have volume
      auto-adjusted to prevent errors or panic
    * Use system's default ffmpeg or use a
      pre-compiled version instead
    * Monitor your live stream

**Currently, Twitch (as of early 2019) still supports IRC for chatting, as well as a few commands, including interacting with bots. However, there is no reason why you couldn't also setup an IRC channel for your live stream as an alternative for those that don't want to create an account for whatever streaming service you decide to use. StreamPi includes a "Wizard" to help you use IRC clients to chat on Twitch.


#####[ Requirements ]#####

Minimum Requirements:
    * gambas3
    * xterm
    * ffmpeg
    * xrandr
    * coreutils
    * gawk or awk

Other requirements:
    * imagemagick (image resizing)
    * curl (for supported stats retrieval)
    * wget (for supported avatar retrieval)
    * youtube-dl (for 'View Live Stream' tool)
    * tor (for "Torify Stream" option)
    * IRC client (to create IRC chat log)
    * xfce4-notifyd (to display follow alerts 
      or part of IRC chat log)
    * Java Runtime Environment version 5 or
      higher (for Voice Changer tool)

Suggestions:
    * moc
    * deadbeef
    * deadbeef-plugins-dev
    * vlc
    * mpv
    * irssi
    * weechat
    * hexchat
    * pidgin
    * konversation

You will need to get an OAuth token for using an IRC client to chat on Twitch. See "Twitch IRC Wizard" in Edit -> Settings -> Tools.


#####[ Privacy ]#####

The developer(s) and contributors of StreamPi do not knowing collect any personal information. However, if the stream monitoring feature is used, it is likely that your public IP address is on record somewhere on a server due to using 'curl' for retrieval of channel statistical information of supported streaming services. This is normal and even typically less privacy concerning than most modern web browsers. And since StreamPi is designed for GNU/Linux-based operating systems, you can install and use a utility called 'lsof' to double-check network connections in the form of 'lsof -P -i -n' via a terminal emulator. You can also add the 'watch' command to the beginning for looping. 

The package 'wget,' in combination with 'curl,' is used to retrieve avatar images from Twitch servers if they exist.

If you are using StreamPi tools to preview a live stream, 'youtube-dl' is used to retrieve the exact URL needed.

Information regarding the storage of StreamPi variables such as private RTMP URL's are stored inside of multiple files within "~/.config/StreamPi/StreamPi."

If using the awk-based translator for chat (https://github.com/soimort/translate-shell/archive/develop.zip), translation services from Google, Bing, Yandex, or Apertium may be used, depending on the user's Settings. The awk-based translation tools are located inside of "~/.config/StreamPi/translator-shell-develop."

If you use the "Torify Stream" feature, it is assumed that you have 'tor' properly setup before-hand. Only the user, as agreed upon by using this software, takes the responsibility for what happens as a result of its usage. Please take note the DISCLAIMER section of the license as it applies to all areas of the software.

Please see the log file for StreamPi in ~/.config/StreamPi/StreamPiLog.txt for details.
