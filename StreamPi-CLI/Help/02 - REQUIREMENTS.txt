StreamPi
by TheOuterLinux

################
# Requirements #
################

I decided to split the requirements list into two parts, one that lists everything and another one that varies from script to script. To just run the StreamPi script itself, the bare minimum for streaming/recording your desktop is only being able to use ffmpeg with h264 in an flv container and coreutils. The other requirements are for the image overlay, Podcast Mode (TTY/console), Settings, Command Center (RunInsideTMUX script) and subscripts, or TwitchBuddy. If you want to run some of Command Center's scripts and don't care for tmux, than you could just as well do so in individual terminals if you'd like.

Master list (to save time):
    1. GNU/Linux or UNIX system
    2. ffmpeg (with h264 and flv container support)
    3. ImageMagick
    4. coreutils
    5. v4l2-ctl
    6. dialog
    7. tmux
    8. acpid
    9. CLI friendly text editor (ex. nano)
   10. fbi
   11. xdg-utils
   12. curl
   13. w3m
   14. wget
   15. ffplay, vlc, vlc-nox, or mpv (preferred)
   16. youtube-dl
   17. caca-utils
   18. alsa or pulseaudio
   19. alsa-utils
   20. pamixer or Command Center will use included binaries instead
   21. A command-line compatible music player such as cmus or moc (preferred)
   22. gawk
   23. exiftools
   24. xsel
   25. zenity
   26. fortune-min
   
#############################################################################
Requirements list split up by scripts:

[$DIR]

    StreamPi - Main part used to stream/local record.flv
    ----------------------------------------------------
        1. GNU/Linux or UNIX system
        2. ffmpeg (with h264 and flv container support)
        3. ImageMagick (if using image overlay)
        4. coreutils
        5. v4l2-ctl (if using webcam in Podcast Mode)
    
    Settings - Change StreamPi settings
    -----------------------------------
        1. GNU/Linux or Unix system
        2. dialog
        3. coreutils
    
    RunInsideTMUX - Command Center
    ------------------------------
        1. Linux/UNIX system
        2. tmux
        3. coreutils
        4. CLI friendly text editor (ex. nano)
        5. alsa-utils (show volume in status)
        6. acpid (show batter % in status)
    
[$DIR/Tools]

    ImageEdit - TTY friendly image editor
    -------------------------------------
        1. GNU/Linux or UNIX system
        2. dialog
        3. coreutils
        4. ImageMagick
        5. fbi
        6. xdg-utils
        7. exiftool
        
    
    Translator - Use to help translate IRC chat or double as a speech synth
    -----------------------------------------------------------------------
        1. GNU/Linux or UNIX system
        2. dialog
        3. coreutils
        4. wget
        5. gawk
        6. tmux
        7. mplayer, mpv, mpg123, or espeak for voice
        8. xsel (for highlight and translate)
        9. zenity (Pop-up box for highlight text translation)
       10. Do not run inside of tmux
        
    TwitchBuddy - Get ahead by seeing what's popular and what your streamer 'niche' is up to
    ----------------------------------------------------------------------------------------
        1. GNU/Linux or UNIX system at runlevel 3 or 5
        2. coreutils
        3. curl
        4. w3m
        5. ffplay, vlc, vlc-nox, or mpv <-preferred (preview streams)

    Fortune - A script that uses 'fortune' to display random sayings for stream breaks/intermission
    -----------------------------------------------------------------------------------------------
        1. GNU/Linux or UNIX system
        2. fortune-min
        
[$DIR/Tools/Command]

    IRC - Twitch currently supports IRC for chat
    --------------------------------------------
        1. GNU/Linux or UNIX system
        2. coreutils
        3. dialog
        4. A command-line compatible IRC client listed within $DIR/Configs/StreamPi/IRC.conf
            *When in doubt, install 'irssi' or 'weechat.'
        
    MediaPlayer - Used in Command Center to preview your stream
    -----------------------------------------------------------
        1. GNU/Linux or UNIX system at runlevel 3 or 5
        2. coreutils
        3. mpv or vlc/vlc-nox (cvlc)
        4. youtube-dl
        5. caca-utils
        
    MusicPlayer - Used in Command Center to play music
    --------------------------------------------------
        1. GNU/Linux or UNIX system
        2. coreutils
        3. alsa or pulseaudio
        4. A command-line compatible music player listed within $DIR/Configs/StreamPi/MUSICPLAYER.conf
            *When in doubt, have 'moc' installed.

    Translator - Use to help translate IRC chat or double as a speech synth
    -----------------------------------------------------------------------
        1. The requirements are the same as 'Translator' mentioned above; the only difference is the dialog for this one tells you not to run one of the menu items inside of tmux because it's ran by default inside of 'RunInsideTMUX' script, aka Command Center.
       
    VolumeControl - Control audio/microphone volume and audio source for stream/local recording.flv
    -----------------------------------------------------------------------------------------------
        1. GNU/Linux or UNIX system
        2. dialog
        3. alsa-utils
        4. pamixer or else it will default to using pre-built binaries inside of $DIR/Tools/PAmix/build_(whatever your system arch is)
        5. alsa or pulseaudio
