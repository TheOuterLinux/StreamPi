StreamPiTorify
by TheOuterLinux

StreamPiTorify is an exact copy of StreamPi, but with a few warnings/disclaimers and 'socks=$SOCKSPROXY' added to the end of each ffmpeg possibility. I did not add the socks proxy URL to the 'Settngs' script on purpose because unless you know what you're doing, you shouldn't use this script anyway. If you look at the contents of StreamPiTorify you will see a bunch of variables at the top. One of those variables is 'SOCKSPROXY.' The default is set to 127.0.0.1:9050 in order to use Tor as the name of this script implies. 

If you are getting errors, you may want to check to see if you have both 'tor' and 'torsocks' installed on your system.

Start tor before launching StreamPiTorify by using:
    sudo service tor start
    
You can check the status of tor using:
    service tor status

This script has been tested on Twitch with a "dummy" account, but after looking everywhere on the account, I could not find location information. To be fair, this is probably a good thing. As far as how other services will handle it, I have no idea. I've only tested it for a few minutes, but it does seem to stream just fine.
