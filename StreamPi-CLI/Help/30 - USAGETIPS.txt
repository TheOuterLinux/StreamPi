StreamPi
by TheOuterLinux

##############
# Usage Tips #
##############

This document is just to help you come up with some ways to get creative with your streaming/recording since StreamPi can only do so much on its own.

| Streaming and recording at the same time
`-----------------------------------------
All you need to do is run StreamPi twice in which one instance streams to the RTMP URL and the other uses a local file path such as "output.flv." The frame rates for both will be lower than usual, but it shouldn't be by much unless you are using image overlays for both on an older computer.

| Desktop and webcam
`-------------------
I do not have an option for this by default because doing so lowers the frame rate more than just opening a webcam application and placing it where ever you need on the screen. If this is something you are interested in, take a look at the following webcam applications and scripts:
    1. guvcview
    2. Cheese
    3. Webcamoid
    4. vlc v4l2:///dev/video0
    5. mplayer tv:// -tv driver=v4l2:width=640:height=480:device=/dev/video0
    6. Hasciicam
    
You can then right-click the top of the window of the webcam previewer and should be able to pin it ("Always on Top") and make it available across all of your workspaces ("Always on Visible Workspace").

!! If you absolutley would rather have a webcam overlay as opposed to an image, you can try the following:

    ffmpeg -f x11grab -video_size 1920x1080 -i $DISPLAY+0,0 -f v4l2 -i /dev/video0 -f pulse -thread_queue_size 1024 -ac 2 -i default -filter_complex '[1:v]colorkey=0x808080:0.01:0.01[ckout];[0:v][ckout]overlay=main_w-overlay_w-2:main_h-overlay_h-2[out]' -map '[out]' -pix_fmt yuv420p -c:v libx264 -c:a aac -b:a 96k -af aresample=resampler=soxr -ar 44100 -preset ultrafast -f flv -threads 0 out.flv
    
    The complex filter setttings are designed for chroma key, but doing it the official way has a much lower frame rate for some reason.


| Bottom corner slide-show
`-------------------------
Some streamers like to show-off their artwork or project photos/screenshots in one of the corners of the screen. In OBS Studio, this is easy; however, StreamPi currently does not have this feature built in. So, you can get around this by installing an image-viewer with features such as a slideshow and shuffle. Unfortunately, most of the popular ones will not "slideshow" without being in fullscreen. You can get around this by trying some of the following:

    1. feh -. -z --slideshow-delay 5 '/path/to/image/folder'
            *Also supports http/https
            
    2. animate -delay 500 -geometry 640x480\! '/path/to/folder/*'
            *Comes from the ImageMagick package
            *500 = 5 seconds between each image
            *"\!" part forces the size of the image to be 480x240 and will skew if needed
    
    3. mpv --image-display-duration=5 --loop --ontop --autofit=480x240 --shuffle '/path/to/image/folder'
            *It's recursive by default
            *Supports whatever youtube-dl supports if using http/https
            *--autofit keeps aspect ratio by resizing width of image without creating black bars but goes no higher than the proposed height.
                If you want the window to stay the same size no matter what, use --geometry= instead.
            *Add --playlist="/path/to/file" with each line as a different image for a more specific slideshow.
            *Add --save-position-on-quit if you need to quit the stream for a while and then start it up again. 
                This will work as long as you use the same /path/to/folder as last time. If you do use this but don't want to resume same position,
                you'll need to add --no-resume-playback.
            *mpv has a man page, but this guide looks a lot nicer: https://mpv.io/manual/master/.


| Music
`------
Personally, I like to MOC music player because it's really light and easy to use; however, it's not for everyone. So, I recommend installing DeadBeeF if you need a GUI music player. It supports a "crap-ton" of file formats, especially after installing 'deadbeef-plugins-dev.' If you need internet radio, all you need to do is find a station using http://www.shoutcast.com and then copy the .m3u link and paste to a terminal as Example: "mpv http://yp.shoutcast.com/sbin/tunein-station.m3u?id=#####". Because mpv uses youtube-dl by default, it should be able to play it. You can also install a program called "mps-youtube" and search & play music from YouTube; it even has playlist support.
