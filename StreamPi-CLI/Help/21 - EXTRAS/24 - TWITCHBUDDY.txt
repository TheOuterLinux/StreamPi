TwitchBuddy
by TheOuterLinux

###############
# TwitchBuddy #
###############

I made this so if you stream to Twitch, you can get an idea of what's being streamed and keep an eye-out on the competition. To edit the categories and channels to monitor, you will have to manually edit the .conf files inside of $DIR/Configs/TwitchBuddy/. Do not edit FEATURED.conf.

curl, w3m, and grep are used to grab information. Because of this, this script is going to be buggy because of item placement changes on web pages. I do not feel like asking people to get an API key and fiddle around with that kind of nightmare. This script also uses ffplay, vlc, cvlc, or mpv to allow a preview of streams and is where the buggy part comes into play; the list can be off by as much as two or three places.

Running this script also creates a directory called "Stats" inside of StreamPi directory in which words are appended to a file and prints out at the end of the script the top words used so far as what data has been collected. This may or may not be used to give a psychological edge in coming up with a stream title. This script is still a work in progress. 
