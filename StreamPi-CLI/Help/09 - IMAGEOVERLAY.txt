StreamPi
by TheOuterLinux

#################
# Image Overlay #
#################

StreamPi's usage without editing the settings is as follows:

    /path/to/StreamPi [rtmp://...] [/path/to/imageoveray.png]
    
Or recording locally use:

    /path/to/StreamPi localrecording.flv [/path/to/imageoveray.png]
    
An image overlay is not required. The image overlay takes up the entire size of the streamed/recorded screen. If the chosen image overaly is smaller than the screen or larger than the screen in X (GUI) environment, it is automatically resized using 'convert' from ImageMagick. If you have 'Span Monitors' turned on, the overlay image blankets across both screens as one image. If you are using an image overlay for the webcam in Podcast Mode (TTY/console), the image is automatically resized to the webcam's dimensions using 'v4l2-ctl.' Image overalys must be RGBA or GIF. If you are not using a webcam but are in TTY/console, image overlay becomes synonymous with "Podcast image" since it isn't "overlaying" on top of anything.

You cannot see the image overlay as it is being streamed/recorded unless you are running the Command Center (RunInsideTMUX script) or $DIR/Tools/Command/MediaPlayer in a separate terminal. If for some reason your ffmpeg build/install is giving you image overlay problems, then you may be able to try this (GUI/X only):
    1. Install and run 'xcompmgr'
    2. Run 'transset 0.5' and [click on main application you're streaming]
    3. Open an image you'd like to always be visible. However, it will appear underneath the transparent main application.
    4. To restore, close the terminal with xcompmgr running or use 'transset 1.0' on any transparent window.

Image overalys are converted to GIF because both PNG and GIF support transparency and because this makes it easier to support animated GIFs in general. Because of this, the colors may not turn out quite the same as the original, especially gradients or items with semi-transparency. This is because GIF takes an "all or none" approach to transparency. So, ordered dithering (o2x2) is used. You can find out more about this here: https://www.imagemagick.org/Usage/quantize/#ordered-dither. Think about it like comic book or newspaper dots but the white part is see-through. You can also find a list of supported dithering for this by running 'identify -list threshold' in a terminal. However, you will have to go into the StreamPi script and change this manually.

If the image overlay is an animated GIF, it is still "converted" to a GIF because the screen sizes vary from computer to computer. And yes, the file size of an animated 1080p GIF can be quite large and sometimes takes a few minutes to convert.

Quitting StreamPi via 'q' as you normally would deletes temporary files associated with the converted copy of the image overlay or podcast image.

If you absolutley would rather have a webcam overlay as opposed to an image, you can try the following:

    ffmpeg -f x11grab -video_size 1920x1080 -i $DISPLAY+0,0 -f v4l2 -i /dev/video0 -f pulse -thread_queue_size 1024 -ac 2 -i default -filter_complex '[1:v]colorkey=0x808080:0.01:0.01[ckout];[0:v][ckout]overlay=main_w-overlay_w-2:main_h-overlay_h-2[out]' -map '[out]' -pix_fmt yuv420p -c:v libx264 -c:a aac -b:a 96k -af aresample=resampler=soxr -ar 44100 -preset ultrafast -f flv -threads 0 out.flv
    
    The complex filter setttings are designed for chroma key, but doing it the official way has a much lower frame rate for some reason.
