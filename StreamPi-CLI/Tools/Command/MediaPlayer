#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
mapfile -t MEDIAPLAYER <"$DIR/../../Configs/StreamPi/MEDIAPLAYER.conf"
STREAMURL="$(cat "$DIR/../../Configs/StreamPi/Settings/Stream/Settings_channelname.conf")"
SLEEP="$(cat "$DIR/../../Configs/StreamPi/Settings/Stream/Settings_previewrefresh.conf")"
ASCII="$(cat "$DIR/../../Configs/StreamPi/Settings/Stream/Settings_previewascii.conf")"
#--[Checks to see if in TTY/Console or if X (GUI) is running]#####
TTYCHECK=$(tty)
TTYCHECK=${TTYCHECK%/*}
TTYCHECK=${TTYCHECK##*/} #If NOT in TTY, TTYCHECK output should be "pts"
#
for i in "${MEDIAPLAYER[@]}" #The order of preference it checks agains in array is least (top) to greatest (bottom)
    do
        if [ -f "$i" ]
        then
            MEDIAPLAYER="$i"
            echo "Media player $i found."
        else
            MEDIAPLAYER=""
        fi
    done
youtube-dl -g "$STREAMURL" > "$DIR/STREAMURL.tmp"
#--[VLC]#####
if [ "$MEDIAPLAYER" = "/usr/bin/cvlc" ] && [ -f "/usr/local/bin/youtube-dl" ]
then
    echo "Using media player VLC to play $STREAMURL..."
    FAILCHECK="$(wc -c < "$DIR/STREAMURL.tmp")"
    while [ "$FAILCHECK" = "0" ]
    do
        rm "$DIR/STREAMURL.tmp"
        echo "Stream must not be playable yet. Trying again in $SLEEP seconds."
        sleep $SLEEP
        STREAMURL="$(cat "$DIR/../../Configs/StreamPi/Settings/Stream/Settings_channelname.conf")"
        youtube-dl -g "$STREAMURL" > "$DIR/STREAMURL.tmp"
        FAILCHECK="$(wc -c < "$DIR/STREAMURL.tmp")"
        echo ""
    done
    rm "$DIR/STREAMURL.tmp"
    if [ "$TTYCHECK" = "pts" ]
    then
        cvlc -A none --preferred-resolution 360 "$(youtube-dl -g "$STREAMURL")"
    else
        cvlc -V caca -A none "$(youtube-dl -g "$STREAMURL")"
    fi
fi
#--[MPV]#####
if [ "$MEDIAPLAYER" = "/usr/bin/mpv" ]
then
    echo "Using media player MPV to play $STREAMURL..."
    FAILCHECK="$(wc -c < "$DIR/STREAMURL.tmp")"
    while [ "$FAILCHECK" = "0" ]
    do
        rm "$DIR/STREAMURL.tmp"
        echo "Stream must not be playable yet. Trying again in $SLEEP seconds."
        echo "Use CTRL+c to cancel."
        sleep $SLEEP
        STREAMURL="$(cat "$DIR/../../Configs/StreamPi/Settings/Stream/Settings_channelname.conf")"
        youtube-dl -g "$STREAMURL" > "$DIR/STREAMURL.tmp"
        FAILCHECK="$(wc -c < "$DIR/STREAMURL.tmp")"
        echo ""
    done
    rm "$DIR/STREAMURL.tmp"
    if [ "$TTYCHECK" = "pts" ] && [ "$ASCII" = "N" ]
    then
        mpv --vo=opengl,x11,tct,caca --no-audio --ytdl-format="best[ext=mp4][height<=?360]" "$STREAMURL"
    else
        RUNLEVELCHECK="$(who -r | cut -d " " -f11)"
        XCHECK="$(ps -e | grep -o X)" #Checks if Xorg is running on another TTY because --vo=drm may crash it.
        if [ "$XCHECK" != "X" ] && [ "$RUNLEVELCHECK" = "5" ] || [ "$ASCII" = "N" ]
        then
            mpv --vo=opengl,drm,x11,tct,caca --no-audio "$STREAMURL" #drm at runlevel 5 allows Command Center on one screen and monitor of the stream on another (laptop)
        else
            mpv --vo=tct,caca --no-audio "$STREAMURL"
        fi
    fi
fi
#--[No Player Found]#####
if [ "$MEDIAPLAYER" = "" ]
then
    dialog --title "Missing Media Player" --msgbox 'It seems as though no compatible command-line player could be found to monitor your stream.\nI suggest installing mpv or vlc-nox, youtube-dl, and caca-utils.' 6 20
fi
