#!/bin/bash
#Translator script (as a frontend) by TheOuterLinux
#Uses: https://github.com/soimort/translate-shell/archive/develop.zip"
# What it does:
#     This script gives you many options for translating text.
# Need:
#     1. Linux/Unix system
#     2. Internet (for translation)
#     3. tmux (Conversation; splits terminal)
#     4. wget
#     5. xsel (for highlight and translate)
#     6. zenity (Pop-up box for highlight text translation)
#     7. gawk
#     9. mplayer, mpv, mpg123, or espeak for voice

if [ ! -f /usr/bin/wget ] || [ ! -f /usr/bin/dialog ] #Checks to see if wget and dialog are installed.
then
    clear
    echo "Sorry, but you are missing wget or dialog. Please make sure both are installed and try again."
    read -p "Press ENTER to quit."
    exit
fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
HEIGHT=0
WIDTH=0
CHOICE_HEIGHT=0
BACKTITLE="Translator"
TITLE="Main Menu"
MENU="Choose one of the following options:"
DEFAULT_DIRECTORY="$DIR/../../Configs/Translator"
#/////[Create directores and settings files (if needed)]/////

if [ ! -d "$DIR/../Configs/Translator" ] #Create directory inside of $DIR/Configs/Translator for settings
then
    mkdir "$DIR/../Configs/Translator"
    echo "en" > "$DIR/../Configs/Translator/NativeLanguage.txt"
    echo "eng" > "$DIR/../Configs/Translator/OCRLanguage.txt"
    echo "google" > "$DIR/../Configs/Translator/Engine.txt"
    echo "Off" > "$DIR/../Configs/Translator/Voice_Type.txt"
    echo "Off" > "$DIR/../Configs/Translator/Voice_Convo.txt"
fi
lang="$(cat "$DIR/../Configs/Translator/NativeLanguage.txt")"
ocr="$(cat "$DIR/../Configs/Translator/OCRLanguage.txt")"
engine="$(cat "$DIR/../Configs/Translator/Engine.txt")"
voice_type="$(cat "$DIR/../Configs/Translator/Voice_Type.txt")"
voice_convo="$(cat "$DIR/../Configs/Translator/Voice_Convo.txt")"
if [ ! -d "$DIR/../Configs/Translator/translate-shell-develop" ] #Retrieve translator for documents.
then
    clear
    echo "The tool needed to translate documents comes from https://github.com/soimort/translate-shell/archive/develop.zip"
    echo "This tool is also used to translate highlighted text via manual keyboard shortcut."
    read -p "Is it ok to install this to $DIR/../../Configs/Translator? (y/n): " answer
    if [ "$answer" == "y" ] || [ "$answer" == "Y" ] || [ "$answer" == "yes" ] || [ "$answer" == "Yes" ] || [ "$answer" == "YES" ]
    then
        cd "$DIR/../Configs/Translator/"
        wget https://github.com/soimort/translate-shell/archive/develop.zip
        unzip "$DIR/../Configs/Translator/develop.zip"
        rm "$DIR/../Configs/Translator/develop.zip"
        cd "$DIR/../Configs/Translator/translate-shell-develop"
        make
        cd "$DIR/../Configs/Translator/"
        echo 'text="$(xsel -o)"' >> notitrans
        echo 'engine="$(cat $DIR/../Configs/Translator/Engine.txt)"' >> notitrans
        echo 'lang="$(cat $DIR/../Configs/Translator/NativeLanguage.txt)"' >> notitrans
        echo 'echo "$($DIR/../Configs/Translator/translate-shell-develop/translate -u Mozilla/5.0 -e "$engine" --no-init -no-ansi -no-bidi -t "$lang" -b "$text")" > /tmp/notitrans' >> notitrans
        echo 'zenity --text-info --title="Translation" --filename=/tmp/notitrans' >> notitrans
        chmod +x "$DIR/../Configs/Translator/notitrans"
        cd $DIR
    else
        echo ""
        echo "The answer you selected was either 'no' or not understood. You can restart the script to try again."
        echo "The 'Type and translate' menu item should still work."
    fi
fi
if [ ! -d /tmp/Translated ] #Create temp directory to work inside
then
    mkdir /tmp/Translated
fi
#[[[[[[ Menu ]]]]]]
MAIN_OPTIONS=(1 "Type and translate"
         2 "Conversation"
         3 "Highlight text and translate"
         4 "Interactive Translate Shell. No flags."
         5 "Settings")
CHOICE_MAINMENU=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${MAIN_OPTIONS[@]}" \
                2>&1 >/dev/tty)
clear
case $CHOICE_MAINMENU in
    1)
			cd $DIR
			if [ "$voice_type" == "On" ]
			then
			    "$DIR/../Configs/Translator/translate-shell-develop/translate" -I -u Mozilla/5.0 -e $engine --no-init -no-ansi -no-bidi -play -t "$lang"
			else
			    "$DIR/../Configs/Translator/translate-shell-develop/translate" -I -u Mozilla/5.0 -e $engine --no-init -no-ansi -no-bidi -t "$lang"
			fi
            cd $DIR
            ./Translator
            exec 3>&-
            ;;
    2)
            if [ ! -f /usr/bin/tmux] #Checks to see if tmux is installed.
            then
                clear
                echo "Sorry, but you are missing tmux. Please install and try again."
                echo "This is required because tmux is used to split the screen."
                read -p "Press ENTER to go back to Main Menu."
                cd $DIR
                ./Translator
                exec 3>&-
            fi
			TMP=$(tty | grep -o "pts")
			clear
			if [ "$voice_convo" == "On" ]
			then
			    speakcheckA="0"
			    while [ $speakcheckA = "0" ]
			    do
			        echo "DISCLAIMER: Not all voices for all languages are supported, nor is accuracy 100%"
			        read -p "Person A (left pane): Do you want a male or female voice?: " personA_voice
			        if [ "$personA_voice" == "male" ] || [ "$personA_voice" == "female" ]
			        then
			            speakcheckA="1"
			        else
			            echo ""
			            echo "You can only select 'male' or 'female.' Please try again."
			        fi
			    done
			fi
			clear
			read -p "Person B (right pane): What is the language code of Person B?: " personB
			if [ "$voice_convo" == "On" ]
			then
			    speakcheckB="0"
			    while [ $speakcheckB = "0" ]
			    do
			        read -p "Person B (left pane): Do you want a male or female voice?: " personB_voice
			        if [ "$personB_voice" == "male" ] || [ "$personB_voice" == "female" ]
			        then
			            speakcheckB="1"
			        else
			            echo ""
			            echo "You can only select 'male' or 'female.' Please try again."
			        fi
			    done
			fi
			if [ "$voice_convo" == "On" ]
			then
				if [ "$TMP" == "pts" ] && [ "$DISPLAY" != "" ]
				then
					SESSION=$RANDOM; tmux new-session -d -s "$SESSION" "$DIR/../Configs/Translator/translate-shell-develop/translate -I -b -play -n $personB_voice -s $lang -t $personB"; tmux select-window -t ${SESSION}:0; tmux split-window "$DIR/../Configs/Translator/translate-shell-develop/translate -I -b -play -n $personA_voice -s $personB -t $lang"; tmux select-layout even-horizontal; tmux set-window-option mouse on; tmux select-pane -t 0; tmux attach-session -t $SESSION; tmux select-window -t ${SESSION};
				else
				    dialog --msgbox "It was detected that you are in TTY.\nThis means you cannot use the mouse to switch panes in tmux.\nTo do this, press CTRL+b and then 'o' to switch between panes." 20 75
			        SESSION=$RANDOM; tmux new-session -d -s "$SESSION" "$DIR/../Configs/Translator/translate-shell-develop/translate -I -b -play -n $personB_voice -s $lang -t $personB"; tmux select-window -t ${SESSION}:0; tmux split-window "$DIR/../Configs/Translator/translate-shell-develop/translate -I -b -play -n $personA_voice -s $personB -t $lang"; tmux select-layout even-horizontal; tmux select-pane -t 0; tmux attach-session -t $SESSION; tmux select-window -t ${SESSION}; bind -r Tab select-pane -t :.+;
				fi
            else
                if [ "$TMP" == "pts" ] && [ "$DISPLAY" != "" ]
				then
                    SESSION=$RANDOM; tmux new-session -d -s "$SESSION" "$DIR/../Configs/Translator/translate-shell-develop/translate -I -b -s $lang -t $personB"; tmux select-window -t ${SESSION}:0; tmux split-window "$DIR/../Configs/Translator/translate-shell-develop/translate -I -b -s $personB -t $lang"; tmux select-layout even-horizontal; tmux set-window-option mouse on; tmux select-pane -t 0; tmux attach-session -t $SESSION; tmux select-window -t ${SESSION};
                else
                	dialog --msgbox "It was detected that you are in TTY.\nThis means you cannot use the mouse to switch panes in tmux.\nTo do this, press CTRL+b and then 'o' to switch between panes.\nAlso, because you have voice turned off, some language output will not display properly.\nYou can change this in Settings." 20 75
                    SESSION=$RANDOM; tmux new-session -d -s "$SESSION" "$DIR/../Configs/Translator/translate-shell-develop/translate -I -b -s $lang -t $personB"; tmux select-window -t ${SESSION}:0; tmux split-window "$DIR/../Configs/Translator/translate-shell-develop/translate -I -b -s $personB -t $lang"; tmux select-layout even-horizontal; tmux select-pane -t 0; tmux attach-session -t $SESSION; tmux select-window -t ${SESSION}; bind -r Tab select-pane -t :.+;
                fi
            fi
            cd $DIR
            ./Translator
            exec 3>&-
            ;;
    3)
            if [ ! -f /usr/bin/zenity] || [ ! -f /usr/bin/xsel] #Checks to see if zenity and xsel are installed.
            then
                clear
                echo "Sorry, but this requires zenity and xsel to work. Please make sure they are installed and try again."
                read -p "Press ENTER to go back to Main Menu."
                cd $DIR
                ./Translator
                exec 3>&-
            fi
			cd $DIR
			dialog --msgbox "To highlight and translate text, open your keyboard shortcuts manager and add an entry for $DIR/../Configs/Translator/notitrans. Afterwards, you will now be able to translate almost any highlightable text (text breaks at '&' symbol) on your system using Google's translator (requires Internet). This does mean you need a desktop environment running for it to work.\nDetails: http://www.webupd8.org/2016/03/translate-any-text-you-select-on-your.html" 20 75
            cd $DIR
            ./Translator
            exec 3>&-
            ;;
    4)
			cd $DIR
			if [ ! -d "$DIR/../../Configs/Translator/translate-shell-develop" ] #Retrieve translator for documents.
            then
                clear
                echo "The tool needed to translate comes from https://github.com/soimort/translate-shell/archive/develop.zip"
                echo "This tool also is used to translate highlighted text via manual keyboard shortcut."
                read -p "Is it ok to install this to $DIR/../../Configs/Translator? (y/n): " answer
                if [ "$answer" == "y" ] || [ "$answer" == "Y" ] || [ "$answer" == "yes" ] || [ "$answer" == "Yes" ] || [ "$answer" == "YES" ]
                then
                    cd "$DIR/../Configs/Translator/"
                    wget https://github.com/soimort/translate-shell/archive/develop.zip
                    unzip "$DIR/../Configs/Translator/develop.zip"
                    rm "$DIR/../Configs/Translator/develop.zip"
                    cd "$DIR/../Configs/Translator/translate-shell-develop"
                    make
                else
                    echo ""
                    echo "The answer you selected was either 'no' or not understood. You can restart the script to try again."
                    echo "The 'Type and translate' menu item should still work."
                fi
            fi
            cd $DIR
			"$DIR/../Configs/Translator/translate-shell-develop/translate" -I
            ./Translator
            exec 3>&-
            ;;
    5)
			cd $DIR
			SUB_1_SETTINGS_OPTIONS=(1 "<-- Main Menu"
            2 "Native language: $lang"
            3 "Change translator engine: $engine"
            4 "Voice")
			CHOICE_SUB_1_SETTINGS=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "Settings" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${SUB_1_SETTINGS_OPTIONS[@]}" \
                2>&1 >/dev/tty)
            case $CHOICE_SUB_1_SETTINGS in
                1)  
                    cd $DIR
                    ./Translator
                    exec 3>&-
                    ;;
                2)
                    clear
                    echo "Your current native language is set to '$lang.'"
                    echo ""
                    echo ".----------------------.---------------------------.---------------------------."
                    echo "| Afrikaans    -    af | Hindi          -       hi | Punjabi         -      pa |"
                    echo "| Albanian     -    sq | Hmong          -      hmn | Querétaro Otomi -     otq |"
                    echo "| Amharic      -    am | Hmong Daw      -      mww | Romanian        -      ro |"
                    echo "| Arabic       -    ar | Hungarian      -       hu | Russian         -      ru |"
                    echo "| Armenian     -    hy | Icelandic      -       is | Samoan          -      sm |"
                    echo "| Azerbaijani  -    az | Igbo           -       ig | Scots Gaelic    -      gd |"
                    echo "| Basque       -    eu | Indonesian     -       id | Serbian (Cyr)   - sr-Cyrl |"
                    echo "| Belarusian   -    be | Irish          -       ga | Serbian (Latin) - sr-Latn |"
                    echo "| Bengali      -    bn | Italian        -       it | Sesotho         -      st |"
                    echo "| Bosnian      -    bs | Japanese       -       ja | Shona           -      sn |"
                    echo "| Bulgarian    -    bg | Javanese       -       jv | Sindhi          -      sd |"
                    echo "| Cantonese    -   yue | Kannada        -       kn | Sinhala         -      si |"
                    echo "| Catalan      -    ca | Kazakh         -       kk | Slovak          -      sk |"
                    echo "| Cebuano      -   ceb | Khmer          -       km | Slovenian       -      sl |"
                    echo "| Chichewa     -    ny | Klingon        -      tlh | Somali          -      so |"
                    echo "| Chinese Simp - zh-CN | Klingon(pIqaD) - tlh-Qaak | Samoan          -      sm |"
                    echo "| Chinese Trad - zh-TW | Korean         -       ko | Sundanese       -      su |"
                    echo "| Corsican     -    co | Kurdish        -       ku | Swahili         -      sw |"
                    echo "| Croatian     -    hr | Kyrgyz         -       ky | Swedish         -      sv |"
                    echo "| Czech        -   cs  | Lao            -       lo | Tahitian        -      ty |"
                    echo "| Danish       -    da | Latin          -       la | Tajik           -      tg |"
                    echo "| Dutch        -    nl | Latvian        -       lv | Tamil           -      ta |"
                    echo "| English      -    en | Lithuanian     -       lt | Tatar           -      tt |"
                    echo "| Esperanto    -    eo | Luxembourgish  -       lb | Telugu          -      te |"
                    echo "| Estonian     -    et | Macedonian     -       mk | Thai            -      th |"
                    echo "| Fijian       -    fj | Malagasy       -       mg | Tongan          -      to |"
                    echo "| Filipino     -    tl | Malay          -       ms | Turkish         -      tr |"
                    echo "| Finnish      -    fi | Malayalam      -       ml | Udmurt          -     udm |"
                    echo "| French       -    fr | Maltese        -       mt | Ukrainian       -      uk |"
                    echo "| Frisian      -    fy | Maori          -       mi | Urdu            -      ur |"
                    echo "| Galician     -    gl | Marathi        -       mr | Uzbek           -      uz |"
                    echo "| Georgian     -    ka | Mongolian      -       mn | Vietnamese      -      vi |"
                    echo "| German       -    de | Myanmar        -       my | Welsh           -      cy |"
                    echo "| Greek        -    el | Nepali         -       ne | Xhosa           -      xh |"
                    echo "| Gujarati     -    gu | Norwegian      -       no | Yiddish         -      yi |"
                    echo "| Haitian Creole -  ht | Pashto         -       ps | Yoruba          -      yo |"
                    echo "| Hausa        -    ha | Persian        -       fa | Yucatec Maya    -     yua |"
                    echo "| Hawaiian     -   haw | Polish         -       pl | Zulu            -      zu |"
                    echo "| Hebrew       -    he | Portuguese     -       pt |                           |"
                    echo "+----------------------+---------------------------+---------------------------'"
                    echo ""
                    read -p "Please enter you native language code (leave blank to ignore): " lang
                    if [ ! "$lang" == "" ]
                    then
                        echo "$lang" > "$DIR/../../Configs/Translator/NativeLanguage.txt"
                    fi
                    ./Translator
                    exec 3>&-
                    ;;
                3)
                    clear
                    echo "Your current engine is $engine."
                    echo ""
                    "$DIR/../Configs/Translator/translate-shell-develop/translate" -S
                    echo ""
                    read -p "Type preferred engine (leave blank to ignore): " engine
                    if [ ! "$engine" == "" ]
                    then
                       echo "$engine" > "$DIR/../Configs/Translator/Engine.txt"
                    fi
                    ./Translator
                    exec 3>&-
                    ;;
                4)
                    cd $DIR
			        SUB_3_SETTINGS_OPTIONS=(1 "<-- Main Menu"
                    2 "Toggle on/off voice for menu items")
			        CHOICE_SUB_3_SETTINGS=$(dialog --clear \
                    --backtitle "$BACKTITLE" \
                    --title "Settings --> Voice" \
                    --menu "$MENU" \
                    $HEIGHT $WIDTH $CHOICE_HEIGHT \
                    "${SUB_3_SETTINGS_OPTIONS[@]}" \
                    2>&1 >/dev/tty)
                    case $CHOICE_SUB_3_SETTINGS in
                        1)  
                            cd $DIR
                            ./Translator
                            exec 3>&-
                            ;;
                        2)  
                            SUB_4_SETTINGS_OPTIONS=(1 "<-- Main Menu"
                            2 "Toggle voice on/off for 'Type and translate' -- $(cat "$DIR/../Configs/Translator/Voice_Type.txt")"
                            3 "Toggle voice on/off for 'Conversation'; Hint: good for VoIP -- $(cat "$DIR/../Configs/Translator/Voice_Convo.txt")")
			                CHOICE_SUB_4_SETTINGS=$(dialog --clear \
                            --backtitle "$BACKTITLE" \
                            --title "Settings --> Voice --> Menu items" \
                            --menu "$MENU" \
                            $HEIGHT $WIDTH $CHOICE_HEIGHT \
                            "${SUB_4_SETTINGS_OPTIONS[@]}" \
                            2>&1 >/dev/tty)
                            case $CHOICE_SUB_4_SETTINGS in
                            1)  
                                cd $DIR
                                ./Translator
                                exec 3>&-
                                ;;
                            2)
                                if [ "$voice_type" == "Off" ]
                                then
                                    echo "On" > "$DIR/../Configs/Translator/Voice_Type.txt"
                                else
                                    echo "Off" > "$DIR/../Configs/Translator/Voice_Type.txt"
                                fi
                                dialog --msgbox "Voice is now $(cat "$DIR/../Configs/Translator/Voice_Type.txt") for 'Type and translate.'" 10 60
                                cd $DIR
                                ./Translator
                                exec 3>&-
                                ;;
                            3)
                                if [ "$voice_convo" == "Off" ]
                                then
                                    echo "On" > "$DIR/../Configs/Translator/Voice_Convo.txt"
                                else
                                    echo "Off" > "$DIR/../Configs/Translator/Voice_Convo.txt"
                                fi
                                dialog --msgbox "Voice is now $(cat "$DIR/../Configs/Translator/Voice_Convo.txt") for 'Conversation'" 10 60
                                cd $DIR
                                ./Translator
                                exec 3>&-
                                ;;
                            esac
                    esac
                    ;;
            esac
            cd $DIR
            ./Translator
            exec 3>&-
            ;;
    255)
            cd $DIR
            exit
            exec 3>&-
            ;;
esac 
