#!/bin/bash
#
# TwitchBuddy
# by TheOuterLinux
#
# This script is only meant to help comliment StreamPi in the sense that it
# gathers category/game and streamer (competitor) information to perhaps
# help you figure out what to stream and when to stream without having
# to open a GUI web browser or app. StreamBuddy configuration (.conf)
# files are located inside of $DIR/../Configs/TwitchBuddy.
#
# REQUIREMENTS:
#     1. Linux/UNIX system
#     2. curl
#     3. w3m
#     4. ffplay, vlc, vlc-nox, or mpv (optional)
#
if [ ! /usr/bin/w3m ]
then
    echo "w3m was not detected on your system. Please install and try again."
    exit
fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" #Helps run scripts from any directory
if [ ! -d "$DIR/../Stats" ]
then
    mkdir "$DIR/../Stats"
fi
clear
SYSTEMTYPE="$(uname -m)" #Check system architecture (ARM, i686, x86_64, etc.)
#####[Checks to see what players are available]#####
PREVIEWON="Y" #Enable/disable stream preview
TTYCHECK=$(tty)
TTYCHECK=${TTYCHECK%/*}
TTYCHECK=${TTYCHECK##*/} #If NOT in TTY, TTYCHECK output should be "pts"
RUNLEVEL="$(who -r | awk -F 'run-level' '{print $2}' | cut -d " " -f2)" #Need to run-level 3 or 5 to play video in TTY
PLAYERCHECK="0"
if [ "$RUNLEVEL" = "3" ] || [ "$RUNLEVEL" = "5" ]
    then
        if [ -f /usr/bin/ffplay ] && [ "$TTYCHECK" = "pts" ] && [ -f /usr/local/bin/youtube-dl ]
        then
            PLAYERCHECK="1"
            BESTPLAYER="ffplay"
        fi
        if [ -f /usr/bin/vlc ] && [ "$TTYCHECK" = "pts" ] && [ -f /usr/local/bin/youtube-dl ]
        then
            PLAYERCHECK="1"
            BESTPLAYER="vlc"
        fi
        if [ -f /usr/bin/cvlc ] && [ "$TTYCHECK" != "pts" ] && [ -f /usr/local/bin/youtube-dl ]
        then
            PLAYERCHECK="1"
            BESTPLAYER="cvlc"
        fi
        if [ -f /usr/bin/mpv ] && [ -f /usr/local/bin/youtube-dl ]
        then
            PLAYERCHECK="1"
            if [ "$SYSTEMTYPE" = "i686" ] || [ "$SYSTEMTYPE" = "x86_64" ] #Checks if using an ARM device (by assumption) and if so, lowers the best quality to 720p and vo=x11
            then
                BESTPLAYER="mpv --vo=opengl,drm,x11,tct,caca --ao=pulse,alsa,jack"
            else
                BESTPLAYER="mpv --vo=x11,tct,caca --ao=alsa --ytdl-format=best[ext=mp4][height<=?720]"
            fi
        fi
fi
PLAYEROVERRIDE="" #Over-ride automatic player settings for stream preview (if enabled); leave blank to ignore.
if [ "$PLAYEROVERRIDE" = "" ] && [ "$PREVIEWON" = "Y" ]
then
    BESTPLAYER="$BESTPLAYER"
else
    BESTPLAYER="$PLAYEROVERRIDE"
fi
#######################################################
mapfile -t CATEGORY <"$DIR/../Configs/TwitchBuddy/CATEGORIES.conf" #Create an array from CATEGORIES.conf
mapfile -t COMPETITOR <"$DIR/../Configs/TwitchBuddy/COMPETITOR.conf" #Create an array from COMPETITOR.conf
FEATURED=$(w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://twitch.tv" | grep -i -B 2 'viewers' | awk 'NR==1 || NR==5 || NR==9 || NR==13 || NR==17 || NR==21 || NR==25 || NR==29 || NR==33 || NR==37 || NR==41 || NR==45')
echo "$FEATURED" > "$DIR/../Configs/TwitchBuddy/FEATURED.conf"
mapfile -t FEATURED <"$DIR/../Configs/TwitchBuddy/FEATURED.conf" #Create an array from FEATURED.conf
echo "# Checking featured..."
echo "######################"
echo ""
for i in "${FEATURED[@]}"
do
    echo "___[$i]___"
    echo -n "Top title: "
    w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://www.twitch.tv/directory/game/$i" | grep -i -A 2 "avatar" | head -n 3 | tail -n 1 | tee -a "$DIR/../Stats/TwitchTitles.txt" #Use w3m mobile UA to grab stream title
    echo -n "Top channel: "
    w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://www.twitch.tv/directory/game/$i" | grep -i -B 2 "$i" | head -n 5 | tail -n 1 #Use w3m mobile UA to the top listed channel in category
    TOPNAME="$(w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://www.twitch.tv/directory/game/$i" | grep -i -B 2 "$i" | head -n 5 | tail -n 1)"
    TOPCHANNEL+=( $TOPNAME )
    echo -n "Average viewers for $i: "
    w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://www.twitch.tv/directory/game/$i" | grep "viewers" | awk '{sub(",","")}1' | cut -d " " -f1 | grep -Eo '[0-9]{1,6}'| awk '{ total += $1 } END { print total/NR }' #Use w3m to grab top 6 viewer counts, awk to remove "," when in thousands, and use awk get an average by adding the numbers on each line and divide by number of lines
    echo ""
done
echo ""
echo "# Checking selected games/categories..."
echo "#######################################"
echo ""
for i in "${CATEGORY[@]}"
do
    echo "___[$i]___"
    echo -n "Top title: "
    w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://www.twitch.tv/directory/game/$i" | grep -i -A 2 "avatar" | head -n 3 | tail -n 1 | tee -a "$DIR/../Stats/TwitchTitles.txt" #Use w3m mobile UA to grab stream title
    echo -n "Top channel: "
    w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://www.twitch.tv/directory/game/$i" | grep -i -B 2 "$i" | head -n 5 | tail -n 1 #Use w3m mobile UA to the top listed channel in category
    TOPNAME="$(w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://www.twitch.tv/directory/game/$i" | grep -i -B 2 "$i" | head -n 5 | tail -n 1)"
    TOPCHANNEL+=( $TOPNAME )
    echo -n "Average viewers for $i: "
    w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://www.twitch.tv/directory/game/$i" | grep "viewers" | awk '{sub(",","")}1' | cut -d " " -f1 | grep -Eo '[0-9]{1,6}'| awk '{ total += $1 } END { print total/NR }' #Use w3m to grab top 6 viewer counts, awk to remove "," when in thousands, and use awk get an average by adding the numbers on each line and divide by number of lines
    echo ""
done
echo ""
echo "# Checking competitor info..."
echo "#############################"
echo ""
for i in "${COMPETITOR[@]}"
do
    echo "___[$i]___"
    w3m -header 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602.1' "https://twitch.tv/$i" | grep "Playing" | tee -a "$DIR/../Stats/TwitchTitles.txt" #Use w3m and grep to display what the streamer (competitor) is playing
    echo -n "Viewer count: "
    curl -s "https://tmi.twitch.tv/group/user/$i/chatters" | awk '/viewers/{y=1;next}y' | sed -n '/]/q;p' | wc -l #Grab line-by-line viewer list and use wc -l to count lines
    echo -n "Follower count: "
    curl -s "https://decapi.me/twitch/followcount/$i"
    echo ""
    echo -n "Subscriber count: "
    errorcheck="$(curl -s "https://decapi.me/twitch/subcount?channel=$i" | grep -o authenticate)" #Check whether or not a streamer is teir 2 partnered with Twitch
    if [ "$errorcheck" = "authenticate" ]
    then
        echo "$i isn't partnered yet."
    else
        curl -s "https://decapi.me/twitch/subcount?channel=$i"
    fi
    echo -n "Uptime: "
    curl -s "https://decapi.me/twitch/uptime?channel=$i"
    echo ""
    echo ""
done
if [ "$PREVIEWON" = "Y" ] && [ "$PLAYERCHECK" = "1" ] #Check if user wants a preview of stream using MPV and that it's installed before trying
then
    LOOP="true"
    while [ "$LOOP" = "true" ]
    do
        number=1
        echo "# Preview streams (It's buggy)"
        echo "##############################"
        echo "#"
        echo "# Featured:"
        for i in "${FEATURED[@]}"
        do
            echo "# $number) Top stream for $i"
            let "number += 1"
        done
        echo "#"
        echo "# Selected Games/Categories:"
        for i in "${CATEGORY[@]}"
        do
            echo "# $number) Top stream for $i"
            let "number += 1"
        done
        for i in "${TOPCHANNEL[@]}"
        do
            X+=( $i ) 
        done
        echo "#"
        echo "# Competitors:"
        for i in "${COMPETITOR[@]}"
        do
            X+=( $i )
            echo "# $number) $i"
            let "number += 1"
        done
        echo "#"
        echo "#################"
        echo ""
        read -p "Select a stream to preview. Enter 'q' to move on: " STREAM
        if [ "$STREAM" = "q" ] || [ "$STREAM" = "Q" ] || [ "$STREAM" = "quit" ] || [ "$STREAM" = "Quit" ] || [ "$STREAM" = "QUIT" ] || [ "$STREAM" = "" ]
        then
            LOOP="0"
        else
            if [ "$BESTPLAYER" = "ffplay" ]
            then
                STREAM=$(($STREAM-1)) # reduce user input by 1 since array starts counting from zero
                FULLURL="$(youtube-dl -g https://twitch.tv/${X[$STREAM]})"
                ffplay -i "$FULLURL"
            else
                STREAM=$(($STREAM-1)) # reduce user input by 1 since array starts counting from zero
                "$($BESTPLAYER https://twitch.tv/${X[$STREAM]})"
            fi
        fi
    done
fi
echo ""
echo "# Word counts derived from top streaming titles and competitors:"
echo "################################################################"
WORDCHOICE="$(sed -e 's/\s/\n/g' < "$DIR/../Stats/TwitchTitles.txt" | sort | uniq -c | sort -nr | head  -10)"
echo "$WORDCHOICE"
